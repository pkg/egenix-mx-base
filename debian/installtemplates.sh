#! /bin/sh

set -e

VERSIONS="$*"
PACKAGES="BeeBase DateTime Queue Proxy Stack TextTools Tools UID URL"

for ver in $VERSIONS; do
    for pkg in $PACKAGES; do
        debpkg=python-egenix-mx$(echo -n $pkg | tr '[:upper:]' '[:lower:]')-doc

        install -d debian/$debpkg/usr/share/doc-base
        sed -e "s/@PKGNAME@/$debpkg/g" \
            -e "s/@FULLNAME@/eGenix mx$pkg/g" \
            -e "s/@PKG@/mx$pkg/g" \
            <debian/docbase.template >debian/$debpkg.doc-base
    done
done
