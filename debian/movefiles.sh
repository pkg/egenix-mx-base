#! /bin/sh

set -e

VERSIONS="$*"
PACKAGES="BeeBase DateTime Queue Proxy Stack TextTools Tools UID URL"

for ver in $VERSIONS; do
    for pkg in $PACKAGES; do
        debpkg=python-egenix-mx$(echo -n $pkg | tr '[:upper:]' '[:lower:]')

        mv debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/mx$pkg/*.h \
           debian/python-egenix-mx-base-dev/usr/include/python$ver/mx

        # Remove duplicated files, move documentation and examples.
        rm -f debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/README \
              debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/LICENSE \
              debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/COPYRIGHT

        install -d debian/$debpkg-doc/usr/share/doc/$debpkg-doc

        if [ -d debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/Examples ]; then
            install -d debian/$debpkg-doc/usr/share/doc/$debpkg-doc/examples

            rm -f debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/Examples/__init__.* \
                  debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/Examples/*.py?
            mv debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/Examples/* \
               debian/$debpkg-doc/usr/share/doc/$debpkg-doc/examples
        fi

        cp mx/$pkg/Doc/mx$pkg.pdf debian/$debpkg-doc/usr/share/doc/$debpkg-doc

        # Remove unwanted cruft.
        rm -rf debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/Examples \
               debian/$debpkg/usr/lib/python$ver/*-packages/mx/$pkg/Doc \
               debian/$debpkg/usr/lib/python$ver/*-packages/mx/*/LICENSE
        find debian/$debpkg -name '*.py[co]' | xargs rm -f
    done
done
